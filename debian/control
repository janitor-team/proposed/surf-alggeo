Source: surf-alggeo
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 autoconf-archive, pkg-config, help2man,
 bison, flex,
 libfl-dev, libgmp-dev, libx11-dev, libjpeg-dev, libtiff5-dev, zlib1g-dev,
 netpbm
Build-Depends-Indep:
 linuxdoc-tools,
 texlive-base-bin,
 texlive-latex-base,
 texlive-latex-recommended,
 texlive-latex-extra,
 texlive-fonts-recommended
Standards-Version: 4.6.0
Homepage: http://surf.sourceforge.net/
Vcs-Git: https://salsa.debian.org/science-team/surf-alggeo.git
Vcs-Browser: https://salsa.debian.org/science-team/surf-alggeo

Package: surf-alggeo
Architecture: any
Depends: surf-alggeo-nox, ${misc:Depends}
Description: visualization of real algebraic geometry
 Surf is a script driven tool to visualize some real algebraic geometry:
 plane algebraic curves, algebraic surfaces and hyperplane sections of
 surfaces.
 .
 The algorithms should be stable enough not to be confused by curve/surface
 singularities in codimension greater than one and the degree of the surface
 or curve. This has been achieved quite a bit. Curves of degree up to 30 and
 surfaces of degree up to 20 have been drawn successfully. However, there are
 examples of curves/surfaces of lower degree where surf fails to produce
 perfect images. This happens especially if the equation of the curve/surface
 is not reduced. Best results are achieved using reduced equations. On the other
 hand, surf displays the Fermat-curves accurately for degree up to 98.
 .
 Surf is free software distributed under the GNU General Public License (GPL).
 .
 This dummy package is meant to allow multi-variant support for surf.

Package: surf-alggeo-nox
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: netpbm
Suggests: surf-alggeo-doc, graphicsmagick-imagemagick-compat
Description: visualization of real algebraic geometry -- no X support
 Surf is a script driven tool to visualize some real algebraic geometry:
 plane algebraic curves, algebraic surfaces and hyperplane sections of
 surfaces.
 .
 The algorithms should be stable enough not to be confused by curve/surface
 singularities in codimension greater than one and the degree of the surface
 or curve. This has been achieved quite a bit. Curves of degree up to 30 and
 surfaces of degree up to 20 have been drawn successfully. However, there are
 examples of curves/surfaces of lower degree where surf fails to produce
 perfect images. This happens especially if the equation of the curve/surface
 is not reduced. Best results are achieved using reduced equations. On the other
 hand, surf displays the Fermat-curves accurately for degree up to 98.
 .
 Surf is free software distributed under the GNU General Public License (GPL).
 .
 This package provides the script driven tool surf without X support.

Package: surf-alggeo-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: surf-alggeo (=${binary:Version}), pdf-viewer
Multi-Arch: foreign
Description: visualization of real algebraic geometry -- manual
 Surf is a script driven tool to visualize some real algebraic geometry:
 plane algebraic curves, algebraic surfaces and hyperplane sections of
 surfaces.
 .
 The algorithms should be stable enough not to be confused by curve/surface
 singularities in codimension greater than one and the degree of the surface
 or curve. This has been achieved quite a bit. Curves of degree up to 30 and
 surfaces of degree up to 20 have been drawn successfully. However, there are
 examples of curves/surfaces of lower degree where surf fails to produce
 perfect images. This happens especially if the equation of the curve/surface
 is not reduced. Best results are achieved using reduced equations. On the other
 hand, surf displays the Fermat-curves accurately for degree up to 98.
 .
 Surf is free software distributed under the GNU General Public License (GPL).
 .
 This package provides the manual for the script driven tool surf; it also
 contains some script samples.
