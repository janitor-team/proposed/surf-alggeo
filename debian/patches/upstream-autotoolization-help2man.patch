Description: autotools: help2man
 Implement the generation of the manpages for the front-end
 program surf with help2man(1); the upstream manpage material
 has been revisited and it is reused as help2man include file.
Origin: debian
Forwarded: submitted
Author: Jerome Benoit <calculus@rezozer.net>
Last-Update: 2015-04-10

--- a/configure.ac
+++ b/configure.ac
@@ -32,6 +32,7 @@
 dnl some standard checks:
 dnl ---------------------
 
+AC_PATH_PROG([HELP2MAN],[help2man])
 AC_PROG_CC
 AC_PROG_CXX
 AC_PROG_LEX
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -1,6 +1,10 @@
 
 bin_PROGRAMS = surf
 
+man_MANS = surf.1
+
+surf_DESCRIPTION = "visualization of algebraic curves and surfaces"
+
 surf_SOURCES = \
 	compfn.cc \
 	cube.cc \
@@ -90,3 +94,18 @@
 	$(ZLIB_LIBS) \
 	-lfl
 
+AM_H2MFLAGS = \
+	--manual="Surf Tool" \
+	--source="surf $(PACKAGE_VERSION)" \
+	--no-info
+
+%.1: %
+	$(HELP2MAN) \
+			-s 1 \
+			$(AM_H2MFLAGS) \
+			-I $(top_srcdir)/man/$*.h2m \
+			-n $(if $($*_DESCRIPTION), $($*_DESCRIPTION), "manual page for $*") \
+			-o $@ \
+		./$<
+
+CLEANFILES = $(man_MANS)
--- a/Makefile.am
+++ b/Makefile.am
@@ -14,8 +14,6 @@
 
 pkgdata_DATA = surf.xpm
 
-man_MANS = surf.1
-
 if MAINTAINER_MODE
 surf.xpm: $(top_builddir)/src/surf $(top_srcdir)/background.pic
 	$(top_builddir)/src/surf --no-gui $(top_srcdir)/background.pic
@@ -23,8 +21,9 @@
 
 dist-hook:
 	cp -prd $(top_srcdir)/examples $(top_distdir)
+	cp -prd $(top_srcdir)/man $(top_distdir)
 
-EXTRA_DIST = background.pic $(man_MANS) $(pkgdata_DATA)
+EXTRA_DIST = background.pic $(pkgdata_DATA)
 
 MAINTAINERCLEANFILES = $(pkgdata_DATA)
 
--- /dev/null
+++ b/man/surf.h2m
@@ -0,0 +1,49 @@
+help2man include file for surf man page
+
+[DESCRIPTION]
+.B surf
+is a tool to render algebraic curves and surfaces. It is script driven
+and has (optionally) a nifty graphical user interface using the Gtk
+widget set.
+
+The only input needed is the equation of an algebraic curve/surface in
+everyday mathematical notation. The output is a (series of) color or
+black and white image(s) in one of several file formats.
+
+.B surf
+also provides a C-style command language which helps working out more
+complicated equations.
+
+The resolution of an image is only bounded by the available
+memory. Since the image is stored as an array of floats and because some
+image processing algorithms need a copy of the image, you need at least
+width*height*12 bytes of virtual memory.
+
+.B surf
+can handle curves/surfaces up to degree 30. The main features include
+.B algebraic
+.BR curves ,
+.B algebraic
+.BR surfaces ,
+.B hyper plane
+.BR sections ,
+.B lines on
+.BR surfaces ,
+.B multiple
+.BR curves/surfaces ,
+.B adaptive anti aliasing
+and
+.BR dithering .
+
+[COPYRIGHT]
+Copyright (C) 1997\-2015 Johannes Gutenberg-Universitaet Mainz
+.br
+Copyright (C) 1996\-1997 Friedrich Alexander Universitaet Erlangen-Nuernberg
+
+[AUTHORS]
+Stephan Endrass,
+Hans Huelf,
+Ruediger Oertel,
+Kai Schneider,
+Ralf Schmitt,
+Johannes Beigel
--- a/src/Options.cc
+++ b/src/Options.cc
@@ -60,9 +60,9 @@
 #ifndef NO_GUI
 		<< std::endl
 		<< "  -n, --no-gui           disable GUI, execute all scripts passed as FILEs" << std::endl
-		<< "  -x, --exec             when running with GUI, execute first script immediately" << std::endl
-		<< "      --progress-dialog  pop up a dialog instead of using a status bar" << std::endl
-		<< "      --auto-resize      resize image windows automatically to image size" << std::endl
+		<< "  -x, --exec             execute scripts passed as FILEs immediately instead of just loaded them" << std::endl
+		<< "      --progress-dialog  pop up a progress dialog instead of using a status bar display during calculations" << std::endl
+		<< "      --auto-resize      resize image windows automatically to image size (without actual screensize check)" << std::endl
 #endif
 		<< std::endl
 		<< "      --disable-auto-extension" << std::endl
--- a/surf.1
+++ /dev/null
@@ -1,105 +0,0 @@
-.TH surf 1 "Mar 31 2000"
-
-.SH NAME
-surf \- visualization of algebraic curves and surfaces
-
-.SH SYNOPSIS
-.B surf
-.B \|\-n\||\|\-\-no-gui
-.IR file \ .\|.\|.
-.br
-.B surf
-.RI [\| Gtk
-.IR options \|]
-.RB [\| \-x \||\| \-\-exec \|]
-.RB [\| \-\-progress-dialog \|]
-.RB [\| \-\-auto-resize \|]
-.IR file \ .\|.\|.
-.br
-.B surf
-.B \|\-\-help
-
-.SH DESCRIPTION
-.B surf
-is a tool to render algebraic curves and surfaces. It is script driven
-and has (optionally) a nifty graphical user interface using the Gtk
-widget set.
-
-The only input needed is the equation of an algebraic curve/surface in
-everyday mathematical notation. The output is a (series of) color or
-black and white image(s) in one of several file formats.
-
-.B surf
-also provides a C-style command language which helps working out more
-complicated equations.
-
-The resolution of an image is only bounded by the available
-memory. Since the image is stored as an array of floats and because some
-image processing algorithms need a copy of the image, you need at least
-width*height*12 bytes of virtual memory.
-
-.B surf
-can handle curves/surfaces up to degree 30. The main features include
-.B algebraic
-.BR curves ,
-.B algebraic
-.BR surfaces ,
-.B hyper plane
-.BR sections ,
-.B lines on
-.BR surfaces ,
-.B multiple
-.BR curves/surfaces ,
-.B adaptive anti aliasing
-and
-.BR dithering .
-
-.SH OPTIONS
-If you run
-.B surf
-with GUI support (i.\|e. without the
-.B \-n\||\|\-\-no-gui
-option) the standard
-.I Gtk options
-are recognized.
-.TP
-.B \-n, \-\-no-gui
-Run without a graphical user interface, just execute the scripts
-given as
-.IR file s
-and exit thereafter.
-
-In case
-.B surf
-has been compiled without GUI support this option does nothing and
-scripts are always executed non-interactively.
-
-.TP
-.B \-x, \-\-exec
-Execute scripts passed as
-.IR file s
-immediately. Otherwise the scripts are just loaded, not executed.
-
-.TP
-.B \-\-progress-dialog
-Pop up a progress dialog instead of the status bar display during
-calculations.
-
-.TP
-.B \-\-auto-reszie
-Automatically resize the image windows to the size of the image.
-Be aware that there's no check whether the image size is bigger than
-your actual screen size.
-
-.TP
-.B \-\-help
-Display usage information.
-
-.SH COPYRIGHT
-Copyright (C)
-1996\-1997 Friedrich Alexander Universitaet Erlangen-Nuernberg,
-1997\-2000 Johannes Gutenberg-Universitaet Mainz
-
-.SH AUTHORS
-Stephan Endrass, Hans Huelf, Ruediger Oertel, Kai Schneider,
-Ralf Schmitt, Johannes Beigel
